#!/bin/bash

parameters="${1}${2}${3}${4}${5}${6}${7}${8}${9}"

Escape_Variables()
{
	text_progress="\033[38;5;113m"
	text_success="\033[38;5;113m"
	text_warning="\033[38;5;221m"
	text_error="\033[38;5;203m"
	text_message="\033[38;5;75m"

	text_bold="\033[1m"
	text_faint="\033[2m"
	text_italic="\033[3m"
	text_underline="\033[4m"

	erase_style="\033[0m"
	erase_line="\033[0K"

	move_up="\033[1A"
	move_down="\033[1B"
	move_foward="\033[1C"
	move_backward="\033[1D"
}

Parameter_Variables()
{
	if [[ $parameters == *"-v"* || $parameters == *"-verbose"* ]]; then
		verbose="1"
		set -x
	fi
}

Path_Variables()
{
	script_path="${0}"
	directory_path="${0%/*}"
}

Input_Off()
{
	stty -echo
}

Input_On()
{
	stty echo
}

Output_Off()
{
	if [[ $verbose == "1" ]]; then
		"$@"
	else
		"$@" &>/dev/null
	fi
}

Check_Environment()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking system environment."${erase_style}

	if [ -d /Install\ *.app ]; then
		environment="installer"
	fi

	if [ ! -d /Install\ *.app ]; then
		environment="system"
	fi

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Checked system environment."${erase_style}
}

Check_Root()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking for root permissions."${erase_style}

	if [[ $environment == "installer" ]]; then
		root_check="passed"
		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Root permissions check passed."${erase_style}
	else

		if [[ $(whoami) == "root" && $environment == "system" ]]; then
			root_check="passed"
			echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Root permissions check passed."${erase_style}
		fi

		if [[ ! $(whoami) == "root" && $environment == "system" ]]; then
			root_check="failed"
			echo -e $(date "+%b %d %H:%M:%S") ${text_error}"- Root permissions check failed."${erase_style}
			echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Run this tool with root permissions."${erase_style}
			Input_On
			exit
		fi

	fi
}

Input_Model()
{
model_list="/     iMac4,1
/     iMac4,2
/     iMac5,1
/     iMac5,2
/     iMac6,1
/     MacBook2,1
/     MacBook3,1
/     MacBook4,1
/     MacBookAir1,1
/     MacBookPro2,1
/     MacBookPro2,2
/     Macmini1,1
/     Macmini2,1
/     MacPro1,1
/     MacPro2,1
/     Xserve1,1
/     Xserve2,1"

model_ati="iMac4,1
iMac5,1
MacBookPro2,1
MacBookPro2,2
MacPro1,1
MacPro2,1
Xserve1,1
Xserve2,1"

	model_detected="$(sysctl -n hw.model)"

	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Detecting model."${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Detected model as $model_detected."${erase_style}

	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ What model would you like to use?"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Input an model option."${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     1 - Use detected model"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     2 - Use manually selected model"${erase_style}

	Input_On
	read -e -p "$(date "+%b %d %H:%M:%S") / " model_option
	Input_Off

	if [[ $model_option == "1" ]]; then
		model="$model_detected"
		echo -e $(date "+%b %d %H:%M:%S") ${text_success}"+ Using $model_detected as model."${erase_style}
	fi

	if [[ $model_option == "2" ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ What model would you like to use?"${erase_style}
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Input your model."${erase_style}
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"$model_list"${erase_style}

		Input_On
		read -e -p "$(date "+%b %d %H:%M:%S") / " model_selected
		Input_Off

		model="$model_selected"
		echo -e $(date "+%b %d %H:%M:%S") ${text_success}"+ Using $model_selected as model."${erase_style}
	fi
}

Input_Volume()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ What volume would you like to use?"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Input a volume number."${erase_style}

	for volume_path in /Volumes/*; do
		volume_name="${volume_path#/Volumes/}"

		if [[ ! "$volume_name" == com.apple* ]]; then
			volume_number=$(($volume_number + 1))
			declare volume_$volume_number="$volume_name"

			echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     ${volume_number} - ${volume_name}"${erase_style} | sort
		fi

	done

	Input_On
	read -e -p "$(date "+%b %d %H:%M:%S") / " volume_number
	Input_Off

	volume="volume_$volume_number"
	volume_name="${!volume}"
	volume_path="/Volumes/$volume_name"
}

Check_Volume_Version()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking system version."${erase_style}
		
		volume_version="$(defaults read "$volume_path"/System/Library/CoreServices/SystemVersion.plist ProductVersion)"
		volume_version_short="$(defaults read "$volume_path"/System/Library/CoreServices/SystemVersion.plist ProductVersion | cut -c-5)"
		
		volume_build="$(defaults read "$volume_path"/System/Library/CoreServices/SystemVersion.plist ProductBuildVersion)"
	
		if [[ ${#volume_version} == "6" ]]; then
			volume_version_short="$(defaults read "$volume_path"/System/Library/CoreServices/SystemVersion.plist ProductVersion | cut -c-4)"
		fi
	
		if [[ $environment == "installer" ]]; then
			system_volume_version="$(defaults read /System/Library/CoreServices/SystemVersion.plist ProductVersion)"
			system_volume_version_short="$(defaults read /System/Library/CoreServices/SystemVersion.plist ProductVersion | cut -c-5)"
		
			system_volume_build="$(defaults read /System/Library/CoreServices/SystemVersion.plist ProductBuildVersion)"
	
			if [[ ${#volume_version} == "6" ]]; then
				system_volume_version_short="$(defaults read /System/Library/CoreServices/SystemVersion.plist ProductVersion | cut -c-4)"
			fi
		fi

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Checked system version."${erase_style}
}

Check_Volume_Support()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking system support."${erase_style}

	if [[ $volume_version_short == "10."[8-9] || $volume_version_short == "10.1"[0-1] ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ System support check passed."${erase_style}
	else
		echo -e $(date "+%b %d %H:%M:%S") ${text_error}"- System support check failed."${erase_style}
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Run this tool on a supported system."${erase_style}

		Input_On
		exit
	fi
}

Check_Volume_parrotgeek()
{
	if [ -e "$volume_path"/Library/LaunchAgents/com.parrotgeek* ]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_warning}"! Your system was patched by another patcher."${erase_style}
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Run this tool on a clean system."${erase_style}

		Input_On
		exit
	fi
}

Clean_Volume()
{
	Output_Off rm -R "$volume_path"/Applications/Utilities/Brightness\ Slider.app

	Output_Off rm -R "$volume_path"/Applications/Utilities/NoSleep.app
	Output_Off rm -R "$volume_path"/System/Library/Extensions/NoSleep.kext
	Output_Off rm -R "$volume_path"/System/Library/PreferencePanes/NoSleep.prefPane
}

Restore_Volume()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Removing boot.efi patch."${erase_style}
		
		chflags nouchg "$volume_path"/System/Library/CoreServices/boot.efi
		rm "$volume_path"/System/Library/CoreServices/boot.efi
	
	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Removed boot.efi patch."${erase_style}


	if [[ $volume_version_short == "10.11" ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Removing input drivers patch."${erase_style}
			
			rm -R "$volume_path"/System/Library/Extensions/AppleHIDMouse.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleIRController.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleTopCase.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleUSBMultitouch.kext
			rm -R "$volume_path"/System/Library/Extensions/AppleUSBTopCase.kext
			rm -R "$volume_path"/System/Library/Extensions/IOBDStorageFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOBluetoothFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOBluetoothHIDDriver.kext
			rm -R "$volume_path"/System/Library/Extensions/IOSerialFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOUSBFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOUSBHostFamily.kext
			rm -R "$volume_path"/System/Library/Extensions/IOUSBMassStorageClass.kext
		
		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Removed input drivers patch."${erase_style}
	fi


	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Removing graphics drivers patch."${erase_style}
		
		rm -R "$volume_path"/System/Library/Extensions/AppleIntelGMA950.kext
		rm -R "$volume_path"/System/Library/Extensions/AppleIntelGMA950GA.plugin
		rm -R "$volume_path"/System/Library/Extensions/AppleIntelGMA950GLDriver.bundle
		rm -R "$volume_path"/System/Library/Extensions/AppleIntelGMA950VADriver.bundle
		rm -R "$volume_path"/System/Library/Extensions/AppleIntelGMAX3100.kext
		rm -R "$volume_path"/System/Library/Extensions/AppleIntelGMAX3100FB.kext
		rm -R "$volume_path"/System/Library/Extensions/AppleIntelGMAX3100GA.plugin
		rm -R "$volume_path"/System/Library/Extensions/AppleIntelGMAX3100GLDriver.bundle
		rm -R "$volume_path"/System/Library/Extensions/AppleIntelGMAX3100VADriver.bundle
		rm -R "$volume_path"/System/Library/Extensions/AppleIntelIntegratedFramebuffer.kext
		rm -R "$volume_path"/System/Library/Extensions/ATI1300Controller.kext
		rm -R "$volume_path"/System/Library/Extensions/ATI1600Controller.kext
		rm -R "$volume_path"/System/Library/Extensions/ATI1900Controller.kext
		rm -R "$volume_path"/System/Library/Extensions/ATIFramebuffer.kext
		rm -R "$volume_path"/System/Library/Extensions/ATIRadeonX1000.kext
		rm -R "$volume_path"/System/Library/Extensions/ATIRadeonX1000GA.plugin
		rm -R "$volume_path"/System/Library/Extensions/ATIRadeonX1000GLDriver.bundle
		rm -R "$volume_path"/System/Library/Extensions/ATIRadeonX1000VADriver.bundle
		rm -R "$volume_path"/System/Library/Extensions/ATISupport.kext
		rm -R "$volume_path"/System/Library/Extensions/GeForce.kext
		rm -R "$volume_path"/System/Library/Extensions/GeForce7xxxGLDriver.bundle
		rm -R "$volume_path"/System/Library/Extensions/GeForce8xxxGLDriver.bundle
		rm -R "$volume_path"/System/Library/Extensions/GeForceGA.plugin
		rm -R "$volume_path"/System/Library/Extensions/GeForceVADriver.bundle
		rm -R "$volume_path"/System/Library/Extensions/NVDANV40Hal.kext
		rm -R "$volume_path"/System/Library/Extensions/NVDANV50Hal.kext
		rm -R "$volume_path"/System/Library/Extensions/NVDAResman.kext

		if [[ ! $model_ati == *$model* ]]; then
			rm "$volume_path"/Library/Preferences/com.apple.PowerManagement.plist
		fi

		defaults delete "$volume_path"/System/Library/LaunchDaemons/com.apple.WindowServer.plist Nice
	
	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Removed graphics drivers patch."${erase_style}


	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Removing audio drivers patch."${erase_style}
		
		rm -R "$volume_path"/System/Library/Extensions/AppleHDA.kext
		rm -R "$volume_path"/System/Library/Extensions/IOAudioFamily.kext
	
	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Removed audio drivers patch."${erase_style}


	if [[ $model == "Macmini1,1" || $model == "Macmini2,1" ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Removing AirPort drivers patch."${erase_style}

			rm -R "$volume_path"/System/Library/Extensions/IO80211Family.kext
			rm "$volume_path"/usr/libexec/airportd

		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Removed AirPort drivers patch."${erase_style}
	fi


	if [[ $volume_version_short == "10.11" ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Removing System Integrity Protection."${erase_style}

			rm -R "$volume_path"/System/Library/Extensions/SIPManager.kext

		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Removed System Integrity Protection."${erase_style}
	fi
}

End()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Thank you for using OS X Patcher."${erase_style}
	
	Input_On
	exit
}

Input_Off
Escape_Variables
Parameter_Variables
Path_Variables
Check_Environment
Check_Root
Input_Model
Input_Volume
Check_Volume_Version
Check_Volume_Support
Check_Volume_parrotgeek
Clean_Volume
Restore_Volume
End